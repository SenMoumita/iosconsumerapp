//
//  nanoInvestCoachingContainer.swift
//  nanoInvest
//
//  Created by Moumita on 12/10/15.
//  Copyright © 2015 mpura. All rights reserved.
//

import Foundation

import UIKit

class nanoInvestCoachingContainer :UIPageViewController{
    
    func getCoachingFirstSession() -> nanoinvestCoachingSessionFirst {
        return storyboard!.instantiateViewControllerWithIdentifier("nanoinvestCoachingSessionFirst") as! nanoinvestCoachingSessionFirst
    }
    
    func getCoachingSecondSession() -> nanoinvestCoachingSessionSecond {
        return storyboard!.instantiateViewControllerWithIdentifier("nanoinvestCoachingSessionSecond") as! nanoinvestCoachingSessionSecond
    }
    
    func getCoachingThirdSession() -> nanoinvestCoachingSessionThird {
        return storyboard!.instantiateViewControllerWithIdentifier("nanoinvestCoachingSessionThird") as! nanoinvestCoachingSessionThird
    }
    func getCoachingFourthSession() -> nanoinvestCoachingSessionFourth {
        return storyboard!.instantiateViewControllerWithIdentifier("nanoinvestCoachingSessionFourth") as! nanoinvestCoachingSessionFourth
    }
    func getCoachingFifthSession() -> nanoinvestCoachingSessionFifth {
        return storyboard!.instantiateViewControllerWithIdentifier("nanoinvestCoachingSessionFifth") as! nanoinvestCoachingSessionFifth
    }
    
    func getLoginSession() -> nanoInvestLoginView {
        
        return storyboard!.instantiateViewControllerWithIdentifier("nanoInvestLoginView") as! nanoInvestLoginView
    }

    
    
    
    
    
    override func viewDidLoad() {
        // view.backgroundColor = .darkGrayColor()
        dataSource = self
        setupPageControl()
        //let appearance = UIPageControl.appearance()
        setViewControllers([getCoachingFirstSession()], direction: .Forward, animated: false, completion: nil)
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.grayColor()
        appearance.currentPageIndicatorTintColor = UIColor.whiteColor()
        appearance.backgroundColor = UIColor(red: 36/255.0, green: 140/255.0, blue: 18/255.0, alpha: 1.0)
       // #ff248c12
    }
    
}

extension nanoInvestCoachingContainer : UIPageViewControllerDataSource {
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        if viewController.isKindOfClass(nanoinvestCoachingSessionFifth) {
            return getCoachingFourthSession()
        }
        else if viewController.isKindOfClass(nanoinvestCoachingSessionFourth) {
        return getCoachingThirdSession()
        }
        else if viewController.isKindOfClass(nanoinvestCoachingSessionThird) {
            return getCoachingSecondSession()
        } else if viewController.isKindOfClass(nanoinvestCoachingSessionSecond) {
            
            return getCoachingFirstSession()
        } else {
            return nil
        }
    }
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        if viewController.isKindOfClass(nanoinvestCoachingSessionFifth){
            
            let loginObject = self.storyboard?.instantiateViewControllerWithIdentifier("nanoInvestLoginView") as? nanoInvestLoginView
            self.presentViewController(loginObject!, animated: true, completion: nil)
            
           // return nil
            
        }else{
        if viewController.isKindOfClass(nanoinvestCoachingSessionFirst) {
            
            return getCoachingSecondSession()
        }
        else if viewController.isKindOfClass(nanoinvestCoachingSessionSecond) {
            
            return getCoachingThirdSession()
        } else if viewController.isKindOfClass(nanoinvestCoachingSessionThird) {
            return getCoachingFourthSession()
        
        }else if viewController.isKindOfClass(nanoinvestCoachingSessionFourth) {
            return getCoachingFifthSession()
        }
        else {
            return nil
        }
        }
        
    return nil
    
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 5
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
//    func pageViewController(pageViewController:UIPageViewController,)
    
//    func pageViewController->(UIPageViewController,, *)pageViewController
//    willTransitionToViewControllers:(NSArray *)pendingViewControllers
//    {
//    EmailTutorialViewController *viewController =
//    [pendingViewControllers objectAtIndex:0];
//    if ([viewController index] == 5)
//    {
//    [self.navigationController popViewControllerAnimated:YES];
//    }
//    }
}

