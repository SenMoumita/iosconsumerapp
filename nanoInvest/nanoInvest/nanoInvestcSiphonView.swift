//
//  nanoInvestcSiphonView.swift
//  nanoInvest
//
//  Created by Moumita on 12/10/15.
//  Copyright © 2015 mpura. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore
import UIKit
import CoreBluetooth

class nanoInvestcSiphonView: UIViewController,CBCentralManagerDelegate, CBPeripheralDelegate{
    
     var popViewController : nanoInvestPopupViewController!
    let centralManager:CBCentralManager!
    var connectingPeripheral:CBPeripheral!
    
    @IBOutlet weak var nanoInvestqrbutton: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //added for bluetooth
        centralManager = CBCentralManager(delegate: self, queue: dispatch_get_main_queue())
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    //var nrfManager:NRFManager!
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.myBle = DeviceBLE()
        
//    self.setRoundedBorder(5, withBorderWidth: 1, withColor: UIColor(red: 0.0, green: 122.0/2550, blue: 1.0, alpha: 1.0), forButton:nanoInvestqrbutton)
        
        
    }

    
    @IBAction func nanoInvestMainmenu(sender: AnyObject) {
        
    }
    @IBAction func nanoInvestLogoff(sender: AnyObject) {
        
        
    }
    
    @IBOutlet weak var nanoInvestreceiveTextLabel: UILabel!
    
    @IBAction func nanoInvestNFCcommunication(sender: AnyObject) {
        
        
      
    }
    @IBAction func nanoInvestBluetoothGeneration(sender: AnyObject) {
        
                
        
    
    
    
    
    }
    
    @IBAction func nanoInvestQrCodeGeneration(sender: AnyObject) {
        
      //  performSegueWithIdentifier("nanoInvestPopupViewController", sender: self)
        
        
        if (UIDevice.currentDevice().userInterfaceIdiom == .Pad)
        {
//           
//             self.popViewController  = self.storyboard?.instantiateViewControllerWithIdentifier("nanoInvestPopupViewController") as? nanoInvestPopupViewController
            
            self.popViewController = nanoInvestPopupViewController(nibName:"PopUpViewController_iPad", bundle:nil )
            
            
            self.popViewController.title = "This is a popup qr view"
           
          
            //UIImage(named: "typpzDemo")
           // self.presentViewController(self.view, animated: true, completion: nil)

            self.popViewController.showInView(self.view, withImage:generateQRImage(), withMessage: "You just triggered a great popup window", animated: true)
        } else
        {
            if UIScreen.mainScreen().bounds.size.width > 320 {
                if UIScreen.mainScreen().scale == 3 {
                    
                    self.popViewController = nanoInvestPopupViewController(nibName:"popupiPhone", bundle:nil)
                    self.popViewController.title = "This is a popup qr view"
                    self.popViewController.showInView(self.view, withImage: UIImage(named: "cSiphon"), withMessage: "Qr code generated for merchant", animated: true)
              
                } else {
                    //for iphone 6Plus
                    self.popViewController = nanoInvestPopupViewController(nibName: "popupiPhone", bundle: nil)
                   self.popViewController.title = "This is a popup qr view"
                    self.popViewController.showInView(self.view, withImage: generateQRImage(), withMessage: "Qr code generated for merchant", animated: true)
                }
            } else {
                
               //for all other type
                
                self.popViewController = nanoInvestPopupViewController(nibName: "nanoInvestPopupViewController", bundle: nil)
                
                
                self.popViewController.title = "This is a popup qr view "
                
                self.popViewController.showInView(self.view, withImage: generateQRImage(), withMessage: "Qr code generated for merchant", animated: true)
                
            }
        }
        
    }
    //MARK:- generate QR code
    func generateQRImage() -> UIImage
    {
        let textData = "Moumita@mobisir".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter!.setValue(textData, forKey: "inputMessage")
        filter!.setValue("H", forKey: "inputCorrectionLevel") // error-correction level: H=30%, Q=25%, M=15% (default), L=7%
        let image = UIImage(CIImage: filter!.outputImage!)
        
        print("Hello new Print with new line",image);
        return image
    }
    
    
    func setRoundedBorder(radius : CGFloat, withBorderWidth borderWidth: CGFloat, withColor color : UIColor, forButton button : UIButton)
    {
        let l : CALayer = button.layer
        l.masksToBounds = true
        l.cornerRadius = radius
        l.borderWidth = borderWidth
        l.borderColor = color.CGColor
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager!){
        
        switch central.state{
        case .PoweredOn:
            print("poweredOn")
            
            let serviceUUIDs:[AnyObject] = [CBUUID(string: "180D")]
            let lastPeripherals = centralManager.retrieveConnectedPeripheralsWithServices(serviceUUIDs)
            
            if lastPeripherals.count > 0{
                let device = lastPeripherals.last as CBPeripheral;
                connectingPeripheral = device;
                centralManager.connectPeripheral(connectingPeripheral, options: nil)
            }
            else {
                centralManager.scanForPeripheralsWithServices(serviceUUIDs, options: nil)
            }
            
        default:
            print(central.state)
        }
    }
    
    func centralManager(central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [NSObject : AnyObject]!, RSSI: NSNumber!) {
        
        connectingPeripheral = peripheral
        connectingPeripheral.delegate = self
        centralManager.connectPeripheral(connectingPeripheral, options: nil)
    }
    
    func centralManager(central: CBCentralManager!, didConnectPeripheral peripheral: CBPeripheral!) {
        
        peripheral.discoverServices(nil)
    }
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverServices error: NSError!) {
        
        if let actualError = error{
            
        }
        else {
            for service in peripheral.services as [CBService]!{
                peripheral.discoverCharacteristics(nil, forService: service)
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverCharacteristicsForService service: CBService!, error: NSError!) {
        
        if let actualError = error{
            
        }
        else {
            
            if service.UUID == CBUUID(string: "180D"){
                for characteristic in service.characteristics as! [CBCharacteristic]{
                    switch characteristic.UUID.UUIDString{
                        
                    case "2A37":
                        // Set notification on heart rate measurement
                        println("Found a Heart Rate Measurement Characteristic")
                        peripheral.setNotifyValue(true, forCharacteristic: characteristic)
                        
                    case "2A38":
                        // Read body sensor location
                        println("Found a Body Sensor Location Characteristic")
                        peripheral.readValueForCharacteristic(characteristic)
                        
                    case "2A39":
                        // Write heart rate control point
                        println("Found a Heart Rate Control Point Characteristic")
                        
                        var rawArray:[UInt8] = [0x01];
                        let data = NSData(bytes: &rawArray, length: rawArray.count)
                        peripheral.writeValue(data, forCharacteristic: characteristic, type: CBCharacteristicWriteType.WithoutResponse)
                        
                    default:
                        println()
                    }
                    
                }
            }
        }
    }
    
    func update(heartRateData heartRateData:NSData){
        
        var buffer = [UInt8](count: heartRateData.length, repeatedValue: 0x00)
        heartRateData.getBytes(&buffer, length: buffer.count)
        
        var bpm:UInt16?
        if (buffer.count >= 2){
            if (buffer[0] & 0x01 == 0){
                bpm = UInt16(buffer[1]);
            }else {
                bpm = UInt16(buffer[1]) << 8
                bpm =  bpm! | UInt16(buffer[2])
            }
        }
        
        if let actualBpm = bpm{
            print(actualBpm)
        }else {
            print(bpm)
        }
    }
    
//    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
//        
//        if let actualError = error{
//            
//        }else {
//            switch characteristic.UUID.UUIDString{
//            case "2A37":
//                update(heartRateData:characteristic.value!)
//                
//            default:
//               
//            }
//        }
//    }

}