//
//  nanoInvestLoginView.swift
//  nanoInvest
//
//  Created by Moumita on 12/10/15.
//  Copyright © 2015 mpura. All rights reserved.
//

import Foundation
import UIKit

class nanoInvestLoginView : UIViewController {
    
    
    @IBAction func nanoInvestLogin(sender: AnyObject) {
        
       // var username = consumerName.text
        //var  userpwd = consumerPwd.text
       
       // toInt()
    
        
        if (consumerName.text!.isEmpty || consumerPwd.text!.isEmpty) {
            
            //show pop up
            
            if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "Invalid Login", message: "Please enter valid user name", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Button", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
            } else {
                // Fallback on earlier versions
                let myAlert = UIAlertView(title: "Invalid Login",
                    message: "Please enter valid user name",
                    delegate: nil, cancelButtonTitle: "Try Again")
                myAlert.show()
            }
           } else {
            
            //show another screen nanoInvestBiometricScanner nanoInvestcSiphonView
            let cSiphonObj = self.storyboard?.instantiateViewControllerWithIdentifier("nanoInvestcSiphonView") as? nanoInvestcSiphonView
            
            self.presentViewController(cSiphonObj!, animated: true, completion: nil)
            self.view.endEditing(true)
            
        }
    }


    @IBAction func nanoInvestReset(sender: AnyObject) {
    
    
        
    }
    @IBOutlet weak var loginlabeltext2: UILabel!
    @IBOutlet weak var loginlabeltext1: UILabel!

    @IBOutlet var consumerPwd: UITextField!
    @IBOutlet var consumerName: UITextField!
}